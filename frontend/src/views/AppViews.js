import React, { useEffect, useState } from 'react'

import { useParams } from 'react-router-dom';

import axios from 'axios';

import ProductPackage from "../components/products/ProductPackage";
import BackButton from "../components/backButton/BackButton"
import StyledCarousel from '../components/hero/HeroSection';
import Brands from '../components/brands/Brands';
import DiscoverField from '../components/discoverField/DiscoverField';
import { Header, ManagementContainer, ProductContainer, InnerContainer } from '../components/management/Management.style';
import ProductDetail from '../components/management/ProductDetail';
import Sidemenu from '../components/management/Sidemenu';
import CreateModal from '../components/modal/CreateModal';
import ProductDetailForm from '../components/products/ProductDetailForm';
import Loading from '../components/loading/Loading';
import { ProductByBrands } from '../components/products/styles/ProductByBrand.style';
import BrandProducts from '../components/products/BrandProducts';
import { ProductPackageContainer } from '../components/products/styles/Product.style';
import { sidebarIcon } from '../assets/imgs/Image';


export function NotFoundView() {
    return (
        <main style={{ padding: "1rem" }}>
            <p>ERROR: PAGE NOT FOUND</p>
        </main>
    )
}

export function Products() {
    return (
        <ProductPackageContainer>
            <BackButton />
            <ProductPackage />
        </ProductPackageContainer>
    );
}

export function Home() {
    const nikeId = 1;
    const adidasId = 2;

    return (
        <div>
            <StyledCarousel />
            <Brands />
            <DiscoverField />
            <div id="discover">
                <ProductByBrands>
                    <BrandProducts id="Nike" brandId={nikeId} label="Nike" />
                    <BrandProducts id="Adidas" brandId={adidasId} label="Adidas" />
                </ProductByBrands>
            </div>
        </div>
    );
}

export function Management() {
    const [isMobile, setMobile] = useState(false);
    const openMenu = () => {
        setMobile(!isMobile);
    }

    // DELETE
    const handleDelete = (e) => {
        const targetId = e.target.id;

        fetch(`http://localhost:8080/api/item/${targetId}`, {
            method: "DELETE",
            headers: {
                'Content-Type': 'application/json',
                'Authorization': localStorage.getItem('accessToken')
            }
        }).then(getDataFromDB())

    }

    // HANDLE GET ALL ITEM FROM DB
    const [data, setData] = useState([]);

    let myHeaders = new Headers();
    myHeaders.append("Authorization", localStorage.getItem("accessToken"));

    let requestOptions = {
        method: 'GET',
        headers: myHeaders,
        redirect: 'follow'
    };

    const getDataFromDB = async () => {
        const response = await fetch("http://localhost:8080/api/item", requestOptions);
        const data = await response.json();
        setData(data);
        setLoading(false);
    }

    // HANDLE SEARCH
    const [searchData, setSearchData] = useState({
        productBrandId: "",
        productId: "",
        productName: "",
        productPrice: 0,
        productDesc: ""
    });

    const [isSearch, setIsSearch] = useState(false);
    const handleSearch = (data) => {
        if (data === "") return;

        setIsSearchAll(false);
        setIsFilterByBrand(false);
        setIsEmpty(false);

        setSearchData(() => {
            return {
                ...data
            }
        })

        setIsSearch(true);
    }

    // HANDLER MODALS
    const [isOpen, setOpenModal] = useState(false);
    const openModal = () => {
        setOpenModal(true);
    }

    const closeModal = () => {
        setOpenModal(false);
        getDataFromDB();
    }

    // ALL ITEM VIEW
    const [isSearchAll, setIsSearchAll] = useState(true);
    const getAllItemView = () => {
        if (isSearchAll === true) return;

        setIsSearch(false);
        setIsFilterByBrand(false);
        setIsEmpty(false);

        setIsSearchAll(true);
    }

    const [loading, setLoading] = useState(true);

    // FILTER BY BRAND
    const [isFilterByBrand, setIsFilterByBrand] = useState(false);
    const [filterItem, setFilterItem] = useState([]);
    const [selectedBrand, setSelectedBrand] = useState("");
    const [isEmpty, setIsEmpty] = useState(false);

    const filterByBrand = async () => {
        setLoading(true);
        setIsSearchAll(false);
        setIsSearch(false);
        setIsEmpty(false);

        const brandId = selectedBrand;
        if (brandId === "") {
            setLoading(false);
            return;
        }

        const dataFromDb = await axios.get(`http://localhost:8080/api/item/public/brand/${brandId}`);
        const brandProducts = await dataFromDb.data;

        if (brandProducts.length === 0) {
            setIsEmpty(true);
        }

        setFilterItem(brandProducts);
        setIsFilterByBrand(true);
        setLoading(false);
    }


    useEffect(() => {
        getDataFromDB();
    }, []);

    return (
        <div className='app-view__management-container'>
            <ManagementContainer>
                <Header>
                    <img onClick={openMenu} src={sidebarIcon} alt='sidebarIcon' />
                    <p>Management</p>
                </Header>
                <InnerContainer>
                    <Sidemenu
                        handleAllItemView={getAllItemView}
                        handleSearch={handleSearch}
                        filterByBrand={filterByBrand}
                        selectedBrand={selectedBrand}
                        setSelectedBrand={setSelectedBrand}
                        isMobile={isMobile}
                        openModal={openModal}
                    />
                    <ProductContainer>
                        {
                            isEmpty && <p id='empty-announce'>No result</p>
                        }
                        {loading ? (<Loading />) : (isFilterByBrand && filterItem.map(item => {
                            return (
                                <ProductDetail
                                    handleDelete={handleDelete}
                                    key={item.id}
                                    productBrand={item.brand.name}
                                    productId={item.id}
                                    productCapacity={item.capacity}
                                    productName={item.name}
                                    productPrice={item.price}
                                />)
                        }))}
                        {loading ? (<Loading />) : (isSearch &&
                            <ProductDetail
                                handleDelete={handleDelete}
                                productBrand={searchData.brand.name}
                                productId={searchData.id}
                                productCapacity={searchData.capacity}
                                productName={searchData.name}
                                productPrice={searchData.price}
                            />)
                        }

                        {/* If not search then show all data */}
                        {loading ? ((<Loading />)) : (isSearchAll && data.map((item) => {
                            return (
                                <ProductDetail
                                    handleDelete={handleDelete}
                                    key={item.id}
                                    productBrand={item.brand.name}
                                    productId={item.id}
                                    productCapacity={item.capacity}
                                    productName={item.name}
                                    productPrice={item.price}
                                />
                            )
                        }))}
                    </ProductContainer>
                </InnerContainer>
            </ManagementContainer>
            <CreateModal
                closeModal={closeModal}
                isOpen={isOpen}
                btnLabel="Create"
            />
        </div>
    );
}

export function ProductDetailView() {
    const { productId } = useParams();
    const [targetItem, setTargetItem] = useState({
        username: "",
        id: 0,
        brand: {
            id: 0,
            brandName: ""
        },
        price: 0,
        capacity: 0,
        description: "",
        url: ""
    });

    const handleChange = (e) => {
        const { name, value } = e.target;

        setTargetItem((prevValue) => {
            return {
                ...prevValue,
                [name]: value
            };
        });
    }

    let myHeaders = new Headers();
    myHeaders.append("Authorization", localStorage.getItem("accessToken"));
    myHeaders.append('Content-Type', 'application/json');
    let requestOptions = {
        method: 'GET',
        headers: myHeaders,
        redirect: 'follow'
    };
    const getItemFromDB = async () => {
        const itemFromDb = await fetch("http://localhost:8080/api/item/" + productId, requestOptions);
        const jItemFromDb = await itemFromDb.json();
        setTargetItem(jItemFromDb);
    }

    useEffect(() => {
        getItemFromDB();
    }, [])

    return (
        <ProductDetailForm handleChange={handleChange} targetItem={targetItem} />
    );
}