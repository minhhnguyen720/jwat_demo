import { AppContainer, GlobalStyle } from "./components/Global";
import Nav from "./components/navbar/Nav";
import {
  BrowserRouter as Router,
  Routes,
  Route
} from "react-router-dom";
import { Home, Management, NotFoundView, ProductDetailView, Products } from "./views/AppViews";
import { AuthProvider } from "./hooks/auth";
import { Login } from "./components/login/Login";
import { RequireAuth } from "./components/requireAuth/RequireAuth";
import Footer from "./components/footer/Footer"

function App() {
  return (
    <AuthProvider>
      <Router>
        <AppContainer>
          <GlobalStyle />
          <Nav />
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/product/:productId" element={<Products />} />
            <Route path="/manage" element={<RequireAuth><Management /></RequireAuth>} />
            <Route path="/manage/:productId" element={<ProductDetailView />} />
            <Route path="/login" element={<Login />} />
            <Route
              path="*"
              element={<NotFoundView />}
            />
          </Routes>
          <Footer />
        </AppContainer>
      </Router>
    </AuthProvider>
  );
}

export default App;