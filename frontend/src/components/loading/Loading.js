import React from 'react'
import styled from 'styled-components'
import {loading as loadingIcon} from '../../assets/imgs/Image'

const LoadingIconContainer = styled.div `
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%,-50%);

    p {
        font-size: 2vmin;
        letter-spacing: 0.3px;
    }
`

export default function Loading() {
  return (
    <LoadingIconContainer className='loading-container'>
        <img src={loadingIcon} alt='loading...'></img>
        <p>Getting data from space...</p>
    </LoadingIconContainer>
  )
}
