import React from 'react'
import { Link } from 'react-router-dom';
import { ReactComponent as ExitIcon } from "../../../assets/imgs/icons/exit-icon.svg"
import { NavigatorContainer as NavContainer } from '../styles/ProductDetailForm.style';

export default function Container({ submitChanges, uploadIcon }) {
    return (
        <NavContainer>
            <Link to="/manage">
                <div className="buttonWrapper">
                    <ExitIcon fill='black' stroke='black' />
                    <p>Back to dashboard</p>
                </div>
            </Link>
            
            <div onClick={submitChanges} className="buttonWrapper">
                <img src={uploadIcon} alt='' />
                <p>Upload changes</p>
            </div>
        </NavContainer>
    );
}