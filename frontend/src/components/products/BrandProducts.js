import React, { useEffect, useState } from 'react'
import { ProductByBrandsContainer } from './styles/ProductByBrand.style';
import axios from 'axios';
import Loading from '../loading/Loading';
import ProductCard from './ProductCard';

export default function BrandProducts(props) {
    const [loading, setLoading] = useState(true);
    const [items, setItems] = useState([]);
    const getProductsByBrand = async () => {
        const dataFromDb = await axios.get(`http://localhost:8080/api/item/public/brand/${props.brandId}`);
        const brandProducts = await dataFromDb.data;
        setItems(brandProducts);
        setLoading(false);
    }

    useEffect(() => {
        getProductsByBrand();
    }, [])

    return loading ? (<Loading />) : (
        <ProductByBrandsContainer>
            <h3 id={props.id}>{props.label}</h3>
                    <div className='inner-container'>
                        {items.map((item) => {
                            return (
                                <ProductCard key={item.id} item={item} />
                            )
                        })}
                    </div>
        </ProductByBrandsContainer>
    )
}
