import styled from "styled-components"

export const ProductDetailFormContainer = styled.div`
    padding-bottom: 2%;
`

export const Header = styled.div`
    padding: 2% 5% 0;

    h1 {
        text-align: center;
        font-weight: 500;
        font-size: 10vmin;
    }
`

export const StyledForm = styled.form`
    display: flex;
    flex-direction: column;
    padding: 3% 15%;
    width: 100%;
    margin-left: 50%;
    transform: translateX(-50%);

    img {
        max-width: 15rem;
        width: 100%;
        image-rendering: pixelated;
        aspect-ratio: 1/1;
    }

    .announce, .failure {
        padding: 3%;
        font-size: 3vmin;

        svg {
            margin-right: 1rem;
        }
    }

    .announce {
        border: 3px solid #16db0f;
        color: #16db0f;

    }

    .failure {
        border: 3px solid #e81710;
        color: #e81710;
    }

    p {
        margin-bottom: 0;
        font-weight: 300;
        font-size: 2.5vmin;
    }

    span {
        font-weight: 500;
    }

    h1 {
        font-size: 5vmin;
        margin-bottom: 1%;
    }

    .buttonWrapper {
        display: block;
        width: 3rem;
        height: 3rem;
        overflow: hidden;
        transition: all 200ms ease-in-out;
        border-radius: 20px;
        border: 3px solid #000000;
        cursor: pointer;
        margin-top: 2rem;

        img {
            margin-right: 7%;
        }

        p {
            font-size: 1.7rem;
        }

        &:hover {
            display: flex;
            flex-direction: row;
            width: 15rem;
            background-color: #000000;

            p {
                color: #ffffff;
            }
        }
    }

    @media screen and (max-width:425px) {
       h1 {
        font-size: 8vmin;
       }
    }
`

export const FormItem = styled.div`
    margin-bottom: 3%;

    label {
        font-size: 2.5vmin;
        margin-right: 1%;
        font-weight: 500;
    }

    input {
        width: 100%;
        font-size: 2.5vmin;
        padding-top: 0.6rem;
        font-weight: 300;
        border: none;
        border-bottom: 1px solid #000000;
        margin-bottom: 0.6rem;
    }

    input:focus {
        outline: none;
        border-bottom: 2px solid #000000;
    }

    textarea {
        display: block;
        outline: auto;
        resize: none;
        width: 100%;
        min-height: 12rem;
        padding: 1%;
        font-size: 2.5vmin;
        font-weight: 300;
    }

    @media screen and (max-width:425px) {
       label {
        font-size: 7vmin;
       }

       input, p , textarea {
        font-size: 6vmin;
       }
    }
`

export const NavigatorContainer = styled.div`
    display: flex;
    justify-content: space-between;

    a {
        text-decoration: none;
    }
`

export const SizeOption = styled.div`
    /* .css-b62m3t-container {
        margin-bottom: 2%;
    }

    .css-1s2u09g-control {
        width: 50%;
    } */

    label {
        display: block;
        font-size: 2.5vmin;
        margin-bottom: 0.5rem;
    }

    button {
        width: 20%;
        margin-left: 1rem;
        padding: 0.5rem;
        font-size: 2vmin;
        background-color: transparent;
        transition: all 200ms ease-in-out;
    
        &:hover {
            background-color: #000000;
            color: #ffffff;
        }
    }

    .removeSize:hover {
        background-color: red;
    }

    select {
        width: 50%;
        padding: 0.5rem;
        font-size: 2vmin;
        border: 1px solid #000000;
    }

    p {
    }
`