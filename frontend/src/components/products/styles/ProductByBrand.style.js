import styled from "styled-components";

export const ProductByBrands = styled.div`
    max-width: 100vw;
    width: 100%;
    padding: 3%;
    height: 100%;
    background-color: #Efefef;

    h3 {
        text-align: center;
        font-size: 7vmin;
    }

    .loading-container {
        position: relative;
        text-align: center;
    }
`

export const ProductByBrandsContainer = styled.div`
    .inner-container {
        display: flex;
        flex-wrap: wrap;
        padding: 5%;
        max-height: 55rem;
        height: 100%;
        overflow-y: auto;
    }

    .card-container {
        margin-right: 2%;
        margin-bottom: 2%;
        background-color: #ffffff;
        border-radius: 10px;
        padding: 1.2rem;
        max-width: fit-content;
        width: 100%;

        .title-container {
            padding-top: 1rem;
            h5 {
                font-size: 3vmin;
                margin-bottom: 0;
            }

            #product-code {
                color: #969696;
                font-size: 1.4vmin;
                margin-bottom: 0;
            }
        }

        p {
            font-size: 2vmin;
            font-weight: 300;
            margin-bottom: 0.5rem;
        }

        button {
            width: 100%;
            padding:1%;
            font-size: 2vmin;
            background-color: #000000;
            color: #ffffff;
            opacity: 0.8;
            transition: all 150ms ease-in-out;

            &:hover {
                opacity: 1;
            }
        }

        img {
            border-radius: 10px;
            object-fit: cover;
            width: 12rem;
            aspect-ratio: 1/1;
        }

        #price {
            color: #EC6516;
            font-size: 4vmin;
            font-weight: 500;
        }
    }
`