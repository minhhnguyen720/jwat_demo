import styled from "styled-components";

export const ProductContainer = styled.div`
    text-align: center;
    width: 100%;
    padding-bottom: 4%;

    img {
        width: 50vmin;
        aspect-ratio: 1/1;
        object-fit: cover;
        image-rendering: pixelated;

        @media screen and (max-width: 425px){
            margin-bottom: 10%;
        }
    }

    #description {
        margin-left: 0;
    }

    #select {
        font-size: 5vmin;
        margin-top: 5%;
    }

    @media screen and (min-width: 1440px) {
        justify-content: center;
    }

    @media screen and (min-width:1250px) {
        & {
            padding: 0 10rem 0 10rem;
        }
    }

    @media screen and (min-width: 768px) {
        & {
            display: flex;
        }

        h1 {
            font-size: 4vmin;
        }

        h2 {
            font-size: 6vmin;
        }
    }

    @media screen and (min-width: 426px) and (max-width: 768px){
        & {
            display: flex;
        }

        h1 {
            font-size: 4vmin;
        }

        h2 {
            font-size: 6vmin;
        }
    }
`

export const ProductName = styled.h1`
    font-size: 8vmin;
    text-transform: uppercase;
    text-align: start;
    font-family: "Oswald",sans-serif;
    font-weight: 400;
`

export const ProductPrice = styled.h2`
    font-size: 13vmin;
    text-align: start;
    font-family: "Oswald",sans-serif;
    font-weight: 500;
    color: #EC6516;
    margin-bottom: 2%;
`

export const Container = styled.div`
    text-align: start;
    margin-left: 5%;
    & {
        @media screen and (min-width: 768px) {
                display: flex;
                flex-direction: column;
        }
    }
`

export const Description = styled.p`
    max-width: 55rem;
    font-size: 2vmin;
    text-align: start;
    margin-top: 0.2rem;
    font-family: "Oswald",sans-serif;
    display: none;

    & {
        @media screen and (min-width: 426px) {
                display: block;
        }
    }
`

export const BuyButton = styled.button`
    max-width: 50rem;
    width:80%;
    margin-top: 1rem;
    padding: 1rem;
    background-color: #ffffff;
    border: 1px solid #000000;
    font-size: 5vmin;
    position: relative;
    font-size: 2.5vmin;
    overflow-x: hidden;
    z-index: 1;

    &::before {
        content: '';
        position: absolute;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        background-color: #000000;
        transform-origin: right;
        transform: scaleX(0);
        transition: transform 200ms ease-in-out;
        z-index: -1;
    }
        
    &:hover::before {
        transform-origin: left;
        transform: scaleX(1);
    }

    &:hover {
        color: #ffffff;
    }

    @media screen and (max-width: 425px) {
        width: 100%;
        font-size: 5vmin;
    }
`

export const ProductPackageContainer = styled.div`
    padding: 5%;
    width: 100%;
    max-width: 100vw;
    height: 45rem;

`