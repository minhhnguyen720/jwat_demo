import { StyledForm, FormItem, Header, ProductDetailFormContainer, SizeOption } from '../products/styles/ProductDetailForm.style'
import { uploadIcon } from "../../assets/imgs/Image";
import { ReactComponent as SuccessIcon } from "../../assets/imgs/icons/success-icon.svg"
import { ReactComponent as FailIcon } from "../../assets/imgs/icons/fail-icon.svg"
import { useEffect, useState } from 'react';
import NavContainer from './productDetailForm/NavigatorContainer';
import { Image } from 'cloudinary-react';
import axios from 'axios';
import { useParams } from 'react-router-dom';
import Loading from '../loading/Loading';


export default function ProductDetailForm({ handleChange, targetItem }) {
    const [success, setSuccess] = useState(false);
    const [failure, setFailure] = useState(false);
    const [loading, setLoading] = useState(true);
    const [cloudName, setCloudName] = useState('');

    const getCloudName = async () => {
        let myHeaders = new Headers();
        myHeaders.append("Authorization", localStorage.getItem("accessToken"));
        myHeaders.append('Content-Type', 'application/json');
        // upload image to Cloudinary
        const cloudNameStream = await fetch("http://localhost:8080/api/cloudinary/cloudName", {
            method: "GET",
            headers: myHeaders
        });

        const cloudName = atob(await cloudNameStream.text());
        setCloudName(cloudName);
    }

    // TEST: use Axios to make GET request
    const [sizeArr, setSizeArr] = useState([]);
    const getAvailableSize = async () => {
        const dataFromDb = await axios.get("http://localhost:8080/api/size", {
            headers: {
                Authorization: localStorage.getItem('accessToken')
            }
        })
        const data = dataFromDb.data;
        setSizeArr(data);
    }

    const submitChanges = (e) => {
        e.preventDefault();

        setFailure(false);
        setSuccess(false);
        setLoading(true);

        if(targetItem.capacity < 0 || targetItem.price < 0) {
            setFailure(true);
            setLoading(false)
            return;
        }

        const item = {
            name: targetItem.name,
            id: targetItem.id,
            brand: {
                id: targetItem.brand.id,
                brandName: targetItem.brand.brandName
            },
            price: targetItem.price,
            capacity: targetItem.capacity,
            description: targetItem.description
        }

        fetch(`http://localhost:8080/api/item/${targetItem.id}`, {
            method: "PUT",
            headers: {
                'Content-Type': 'application/json',
                'Authorization': localStorage.getItem('accessToken')
            },
            body: JSON.stringify(item)
        }).then(response => {
            if (response.status === 200) {
                setSuccess(true);
            } else {
                setFailure(true);
            }
        }).finally(
            setLoading(false)
        )
    }

    const [selectedSize, setSelectedSize] = useState("1");
    const handleSelectedSize = (e) => {
        setAnnounce(false);
        setLoaded(false);
        setSelectedSize(e.target.value);
    }

    const [showAnnouce, setAnnounce] = useState(false);
    const [loaded, setLoaded] = useState(false);
    const addSize = async (e) => {
        e.preventDefault();
        await fetch(`http://localhost:8080/api/item/${targetItem.id}/size/${selectedSize}`, {
            method: "PUT",
            headers: {
                "Authorization": localStorage.getItem("accessToken")
            }
        }).then(res => {
            setAnnounce(true);
            if (res.ok) {
                setLoaded(true);
                getAvailableSizeArray();
            }
        })
    }

    // HANDLE AVAILABLE SIZES
    const [availableSizeArray, setAvailableSizeArray] = useState([]);
    const [selectedAvailableSize, setSelectedAvailableSize] = useState("");
    const urlParam = useParams();
    const getAvailableSizeArray = async () => {
        const productId = urlParam.productId;
        const sizeArrJson = await axios.get(`http://localhost:8080/api/item/${productId}/sizes`, {
            headers: {
                Authorization: localStorage.getItem('accessToken')
            }
        });
        const sizeAvailableArr = await sizeArrJson.data;
        const header = { id: "", value: "Select size" }
        sizeAvailableArr.unshift(header);
        setAvailableSizeArray(sizeAvailableArr);
    }

    const handleSelectedAvailableSize = (e) => {
        setSelectedAvailableSize(e.target.value);
    }

    const removeSizeFromItem = (e) => {
        e.preventDefault();
        const current = selectedAvailableSize;
        setLoading(true);

        if (availableSizeArray.length === 0 || current === "") {
            setSelectedAvailableSize("");
            return;
        }

        const productId = urlParam.productId;
        fetch(`http://localhost:8080/api/item/${productId}/size/remove/${current}`, {
            method: "PUT",
            headers: {
                "Authorization": localStorage.getItem("accessToken")
            }
        }).then(res => {
            if (res.ok) {
                getAvailableSizeArray();
                setSelectedAvailableSize("");
            } else {
                console.log(res);
            }
        }).finally(setLoading(false))
    }

    useEffect(() => {
        getCloudName();
        getAvailableSize();
        getAvailableSizeArray();
        setLoading(false);
    }, [])


    return (loading ? ((<Loading />)) :
        (<ProductDetailFormContainer>
            <Header>
                <h1>Details</h1>
            </Header>
            <StyledForm>
                {success &&
                    <div className='announce'>
                        <SuccessIcon /> Upload success
                    </div>
                }
                {failure &&
                    <div className='failure'>
                        <FailIcon /> Upload fail. Please check your data input again.
                    </div>
                }
                <h1>Product information</h1>
                <Image cloudName={cloudName} publicId={targetItem.url} />

                <FormItem>
                    <label htmlFor='itemName'>Name:</label>
                    <input autoComplete='off' onChange={handleChange} type='text' name='name' id='itemName' value={targetItem.name} />
                    <p><span>ID: </span>{targetItem.id}</p>
                </FormItem>
                <FormItem>
                    <label htmlFor='description'>Description</label>
                    <textarea onChange={handleChange} type='text' name='description' id='description' value={targetItem.description} />
                </FormItem>

                <h1>Brand</h1>
                <FormItem>
                    <p><span>Brand id: </span>{targetItem.brand.id}</p>
                    <p><span>Brand name: </span>{targetItem.brand.name}</p>
                </FormItem>

                <h1>Storage detail</h1>
                <FormItem>
                    <label htmlFor='price'>Price($)</label>
                    <input autoComplete='off' onChange={handleChange} type='number' name='price' id='price' value={targetItem.price} />
                </FormItem>

                <FormItem>
                    <label htmlFor='capacity'>Capacity</label>
                    <input autoComplete='off' onChange={handleChange} type='number' name='capacity' id='capacity' value={targetItem.capacity} />
                </FormItem>

                <h1>Sizes</h1>
                <SizeOption>
                    <label>Available size</label>
                    <select value={selectedAvailableSize} onChange={handleSelectedAvailableSize}>
                        {availableSizeArray.map(sizeItem => {
                            return (
                                <option key={sizeItem.id} value={sizeItem.id}>{sizeItem.value}</option>
                            )
                        })}
                    </select>
                    <button onClick={removeSizeFromItem} className="removeSize">Remove size</button>
                    <p>Current selection: {selectedAvailableSize}</p>
                </SizeOption>

                <SizeOption>
                    <label>Assign size</label>
                    <select value={selectedSize} onChange={handleSelectedSize}>
                        {sizeArr.map(sizeItem => {
                            return (
                                <option value={sizeItem.id}>{sizeItem.value}</option>
                            )
                        })}
                    </select>
                    <button onClick={addSize}>Add size</button>
                    {showAnnouce ? (loaded ? <p>Add size completed</p> : <p>Add size failed</p>) : ""}
                </SizeOption>

                <NavContainer
                    submitChanges={submitChanges}
                    uploadIcon={uploadIcon}
                />
            </StyledForm>
        </ProductDetailFormContainer>)
    );
}