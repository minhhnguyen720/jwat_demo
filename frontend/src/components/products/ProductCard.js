import React from 'react'
import { Image } from 'cloudinary-react'
import { Link } from 'react-router-dom'

// temporary hard coding cloudName, will be update get more dynamic ability in future
export default function ProductCard({ item }) {
    return (
        <div className='card-container'>
            <Image cloudName="dxlepaw47" publicId={item.url} />
            <div className='title-container'>
                <h5>{item.name}</h5>
                <p id='product-code'>Product code: {item.id}</p>
            </div>
            <p id='price'>{item.price}$</p>
            <Link to={"/product/" + item.id}>
                <button>Check out</button>
            </Link>
        </div>
    )
}
