import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom';

import { Container, ProductContainer, ProductName, ProductPrice, Description, BuyButton } from "../products/styles/Product.style"
import axios from 'axios';
import { Image } from 'cloudinary-react';
import { SizeOption } from './styles/ProductDetailForm.style';

export default function ProductPackage(props) {
    const urlId = useParams();
    const productId = urlId.productId;

    const [item, setItem] = useState({});
    const getProduct = async () => {
        const dataFromDb = await axios.get(`http://localhost:8080/api/public/item/${productId}`);

        const jItem = await dataFromDb.data;
        setItem(jItem);
    }

    const [sizeArr,setSizeArr] = useState([]);
    const getSizes = async () => {
        const dataFromDb = await axios.get(`http://localhost:8080/api/public/item/${productId}/sizes`);
        const jSizeArr = await dataFromDb.data;
        setSizeArr(jSizeArr);
    }


    useEffect(() => {
        getProduct();
        getSizes();
    }, [])

    return (
        <ProductContainer>
            <Image cloudName="dxlepaw47" publicId={item.url} />
            <Container>
                <ProductName>{item.name}</ProductName>
                <Container id='description'>
                    <Description>
                        {item.description}
                    </Description>
                </Container>
                <ProductPrice>{item.price}$</ProductPrice>
                <SizeOption>
                    <select id='select'>
                        {sizeArr.map(sizeItem => {
                            return (
                                <option value={sizeItem.id}>{sizeItem.value}</option>
                            )
                        })}
                    </select>
                </SizeOption>
                <BuyButton>
                    Add to cart
                </BuyButton>
            </Container>
        </ProductContainer>
    );
}