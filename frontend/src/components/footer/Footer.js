import React, { useState } from 'react'
import { FooterContainer } from './Footer.style'

export default function Footer() {
    // 👇️ Get current Year
    const currentYear = new Date().getFullYear();

    const [year] = useState(currentYear);

    return (
        <FooterContainer>
            <div className='inner-container'>
                <h2>REKICK.</h2>
                <h4>Made by:</h4>
                <ul>
                    <li>willie</li>
                    <li>nguyen tien</li>
                    <li>xuan mai</li>
                </ul>
                <p>Check out project source code at <a href='https://gitlab.com/minhhnguyen720/jwat_demo'>here</a></p>
                <p>Copyright @{year}</p>
            </div>
        </FooterContainer>
    )
}
