import styled from "styled-components"

export const FooterContainer = styled.div`
    background-color: #000000;
    width: 100%;

    .inner-container {
        display: flex;
        justify-content: center;
        align-items: center;
        flex-direction: column;
        padding: 1% 10%;
        width: 80%;
        margin: auto;
        color: #ffffff;

        h2 {
            font-family: 'Montserrat';
            font-size: 4vmin;
            letter-spacing: 2px;
            background: #B8E1FC;
            background: linear-gradient(to top left,#B8E1FC 0%, #231421 100%);
            -webkit-background-clip: text;
            -webkit-text-fill-color: transparent;
        }

        h4 {
            font-size: 2.5vmin;
            font-weight: 400;
        }

        ul {
            list-style: none;
            text-align: center;
            padding-left: 0;
            li {
                font-size: 2vmin;
                font-weight: 200;
                opacity: 0.7;
                cursor: pointer;
                transition: 150ms ease-in-out;

                &:hover {
                    opacity: 1;
                }
            }
        }

        p {
            font-size: 1.7vmin;
            font-weight: 200;
            a {
                text-decoration: none;
                color: #ffffff;
                border-bottom: 1px solid #ffffff;
                transition: 150ms ease-in-out;
                opacity: 0.7;
                &:hover {
                    opacity: 1;
                }
            }
        }

        @media screen and (max-width: 425px) {
            h2 {
                font-size: 7vmin;
            }

            h4 {
                font-size: 4.5vmin;
            }

            ul li{
                font-size: 4vmin;
            }

            p {
                font-size: 3.5vmin;
                width: 100%;
                text-align: center;
            }
        }
    }
`