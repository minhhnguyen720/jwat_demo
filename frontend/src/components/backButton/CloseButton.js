import React from 'react';
import { closeIcon } from '../../assets/imgs/Image';

export default function CloseButton(props) {
    return (
        <div className="buttonContainer">
            <button onClick={props.closeModal}>
                <img src={closeIcon} alt='' />
            </button>
        </div>
    );
}