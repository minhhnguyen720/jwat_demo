import { useState } from 'react'
import { useAuth } from '../../hooks/auth';
import { useNavigate, useLocation } from 'react-router-dom';
import { LoginFormContainer } from './Login.style';
import { bg2 } from '../../assets/imgs/Image'

export const Login = () => {
    const [user, setUser] = useState({
        username: "",
        password: ""
    });
    const [failure, setFailure] = useState(false);

    const auth = useAuth();
    const navigate = useNavigate();
    const location = useLocation();

    const handleChange = (e) => {
        const { name, value } = e.target;
        setFailure(false);

        setUser((prevValue) => {
            return ({
                ...prevValue,
                [name]: value
            })
        })
    }

    const redirectPath = location.state?.path || '/'

    const handleLogin = (e) => {
        e.preventDefault();

        let myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        let raw = JSON.stringify({
            "username": user.username,
            "password": user.password
        });

        let requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };

        fetch("http://localhost:8080/login", requestOptions)
            .then(response => {
                if (response.ok) {
                    auth.login(user);
                    // Prevent user return to login page after login
                    navigate(redirectPath, { replace: true });
                    return response.headers.get('Authorization');
                } else {
                    setFailure(true);
                    throw Error(response.status)
                }

            })
            .then(result => {
                localStorage.setItem("accessToken", result)
            })
            .catch(error => {
                setFailure(true);
                console.log('error', error)
            });
    }

    return (
        <LoginFormContainer>
            <div className='bg'>
                <img src={bg2} alt='bg' />
            </div>
            <div className='login'>
                <h1>Welcome back</h1>

                {failure &&
                    <div className='failure'>
                        <p>Wrong username or password</p>
                    </div>
                }
                <input
                    name='username'
                    type='text'
                    onChange={handleChange}
                    value={user.username}
                    autoComplete='off'
                    placeholder="username"
                />

                <input
                    name='password'
                    type='password'
                    value={user.password}
                    onChange={handleChange}
                    placeholder="password"
                />
                <button onClick={handleLogin}>Login</button>
            </div>
        </LoginFormContainer>
    );
}
