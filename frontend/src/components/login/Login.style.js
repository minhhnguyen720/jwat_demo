import styled from "styled-components";

export const LoginFormContainer = styled.div`
    position: relative;
    width: 100%;
    height: 100vh;
    display: flex;
    /* justify-content: space-between; */
    
    .bg {
        position: relative;
        width: 100%;
        height: 100%;

    }

    .bg img {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        object-fit: cover;
    }

    .login {
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: center;
        width: 70%;
        height: 100%;
        padding: 3%;

        .failure {
            border: 3px solid #e81710;
            color: #e81710;
            padding: 5%;
            text-align: center;
            font-size: 4vmin;
            margin-bottom: 1rem;
            margin-top: 1rem;
            p {
                margin-bottom: 0;
            }
        }

        h1 {
            font-size: 6vmin;
            margin-bottom: 3rem;
            text-align: center;
            font-weight: 400;
            width: 100%;
        }

        input {
            width: 100%;
            padding: 0.2rem 0.5rem;
            font-size: 3vmin;
            font-weight: 300;
            margin-bottom: 1rem;
            transition: all 200ms ease-in-out;
        }

        button {
            margin-top: 1rem;
            width: 100%;
            padding: 0.3rem;
            font-size: 3vmin;
            position: relative;
            z-index: 1;
            background-color:#ffffff;
            border: 1px solid #000000;
        }

        button::before {
            content: '';
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            background-color: #000000;
            z-index: -1;

            transform-origin: right;
            transform: scaleX(0);
            transition: transform 200ms ease-in-out;
        }
            
        button:hover::before {
            transform-origin: left;
            transform: scaleX(1);
        }

        button:hover {
            color: #ffffff;
        }
    }

    @media screen and (max-width: 768px) {
        .bg {
            position: absolute;
            width: 100%;
            height: 100%;
            filter: grayscale(0.3);
            filter: blur(1px);
        }

        .login {
            position: fixed;
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;
            width: 60%;
            height: 50%;
            top: 50%;
            left: 50%;
            transform: translate(-50%,-50%);
            background: rgb(43,43,43,0.9);
            z-index: 10;
            border-radius: 50px;

            h1 {
                font-size: 7vmin;
                color: #ffffff;
            }

            input {
                width: 80%;
                font-size: 4vmin;
            }

            button {
                width: 80%;
                font-size: 4vmin;
            }
        }
    }

    @media screen and (max-width: 425px) {
        .login {
            width: 100%;
            height: 50%;

            input {
                font-size: 7vmin;
            }

            button {
                font-size: 7vmin;
            }

            h1 {
                font-size: 10vmin;
            }
        }
    }
`