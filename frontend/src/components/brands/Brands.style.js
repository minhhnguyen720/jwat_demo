import styled from "styled-components" 

export const BrandsContainer = styled.div `
    display: flex;
    justify-content: space-between;
    padding: 3% 7%;
`

export const BrandImg = styled.img`
    width: 11vmin;
    aspect-ratio: 1/1;
    opacity: 0.8;
    transition: opacity 200ms ease-in-out;
    cursor: pointer;

    &:hover {
        opacity: 1;
    }
`