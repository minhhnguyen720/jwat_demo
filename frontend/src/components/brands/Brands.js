import React from 'react'
import { BrandsLogo } from '../../assets/Dummy';
import { BrandImg, BrandsContainer } from './Brands.style';

export default function Brands() {
    return(
        <BrandsContainer>
            {
                BrandsLogo.map((logo,index) => {
                    return(
                        <a href={"#"+logo.name}><BrandImg key={index} src={logo.src} alt=''/></a>
                    );
                })
            }
        </BrandsContainer>
    );
}