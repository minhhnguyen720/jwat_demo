import React, { useState } from 'react'

import { Navbar, Header, Nav as NavContainer, LogoLink } from "./Navbar.style";
import { NavLink } from "./NavLink";
import { burgerIcon } from "../../assets/imgs/Image";
import BurgerMenu from "./icons/BurgerMenu";
import { useAuth } from '../../hooks/auth';

export default function Nav() {
    const [rotate, setRotate] = useState(false);

    const handleRotate = () => {
        setRotate(!rotate);
    }

    const closeRotate = () => {
        setRotate(false);
    }

    const auth = useAuth();

    return (
        <>
            <BurgerMenu
                src={burgerIcon}
                onClick={handleRotate}
                $isRotate={rotate}
            />
            <Header>
                <LogoLink to="/">
                    <h1>REKICKS.</h1>
                </LogoLink>
                <Navbar>
                    <NavContainer $rotate={rotate}>
                        <NavLink onClick={closeRotate} to="/" label="Home" />
                        <NavLink onClick={closeRotate} to="/contact" label="Contact" />
                        <NavLink onClick={closeRotate} to="/manage" label="Manage" />
                        {
                            !auth.user && (
                                <NavLink onClick={closeRotate} to="/login" label="Log in">
                                    Login
                                </NavLink>
                            )
                        }
                    </NavContainer>
                </Navbar>
            </Header>
        </>
    );
}