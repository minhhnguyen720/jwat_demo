import React from 'react'
import { IconContainer } from './style/Icon.style'
import { ReactComponent as BurgerIcon } from "../../../assets/imgs/icons/burger-menu.svg";

export default function BurgerMenu(props) {

    return (
        <IconContainer onClick={props.onClick} $isRotate={props.$isRotate}>
            <BurgerIcon stroke={"white"}/>
        </IconContainer>
    );
}