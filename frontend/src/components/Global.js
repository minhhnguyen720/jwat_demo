import styled, { createGlobalStyle } from 'styled-components';

export const GlobalStyle = createGlobalStyle`
    * {
        box-sizing: border-box;
        margin: 0;
        padding: 0;
        font-family: 'Oswald', sans-serif;
    }

    html {
        scroll-behavior: smooth;
    }
`

export const AppContainer = styled.div`
    height: 100vh;
    max-width: 100vw;
    width: 100%;
    overflow-x: hidden;
    position: relative;

    @media screen and (max-width: 560px) {
        padding-left: 0;
    }

`