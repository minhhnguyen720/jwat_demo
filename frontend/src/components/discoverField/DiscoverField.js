import React from 'react'
import { DiscoverFieldContainer, StyledCarouselInner } from './DiscoverField.style';

import { CarouselData } from '../../assets/Dummy';
import { DiscoverFieldData } from './DiscoverFieldData';
import { StyledImg} from '../hero/HeroSection.style';

export default function DiscoverField() {
    const intervalValue = 3000;

    return (
        <DiscoverFieldContainer>
            <StyledCarouselInner fade>
                {DiscoverFieldData.map((item) => {
                    return (
                        <StyledCarouselInner.Item key={item.src} interval={intervalValue}>
                            <StyledImg
                                src={item.src}
                                alt=""
                            />
                        </StyledCarouselInner.Item>
                    );
                })}
            </StyledCarouselInner>

            <div className="sideField">
                <h1>Check out new summer collection</h1>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Amet consectetur adipiscing elit pellentesque habitant morbi tristique senectus et.</p>
                <button>Discover now</button>
            </div>

        </DiscoverFieldContainer>
    );
}