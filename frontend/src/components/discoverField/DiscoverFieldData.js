import {chunkyball,chuckTaylor,sbDunk,stan} from "../../assets/imgs/Features"

export const DiscoverFieldData = [
    {
        src: chunkyball
    },
    {
        src: chuckTaylor
    },
    {
        src: sbDunk
    },
    {
        src: stan
    },
]