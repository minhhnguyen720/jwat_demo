import React, { useState } from 'react'
import { StyledSearchBar } from './Management.style'
import { ReactComponent as SearchIcon } from '../../assets/imgs/icons/search-icon.svg';

export default function SearchBar(props) {
    const [searchValue, setSearchValue] = useState("");

    let myHeaders = new Headers();
    myHeaders.append("Authorization", localStorage.getItem("accessToken"));
    myHeaders.append('Content-Type', 'application/json' );

    const handleSearch = async () => {
        const dataFromDB = await fetch(`http://localhost:8080/api/item/${searchValue}`,{
            method: "GET",
            headers: myHeaders
        });
        const data = await dataFromDB.json();
        
        props.handleSearch(data);
        setSearchValue("");
    }

    const handleChange = (e) => {
        const {value} = e.target;
        setSearchValue(value);
    }

    return (
        <StyledSearchBar
            isMobile={props.isMobile}
            action={props.url}
            method={props.method}
        >
            <button onClick={handleSearch}>
                <SearchIcon fill={props.fill} />
            </button>
            <input
                onChange={handleChange}
                autoComplete="off"
                placeholder="Product ID"
                type="text"
                id="productId"
                name="search"
                value={searchValue}
            />
        </StyledSearchBar>
    )
} 
