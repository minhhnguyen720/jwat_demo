import React, { useState } from 'react'

import { SidemenuContainer } from './Management.style';
import SearchBar from './SearchBar';
import { ReactComponent as AddIcon } from '../../assets/imgs/icons/add-icon.svg';
import { ReactComponent as ManageIcon } from '../../assets/imgs/icons/manage-icon.svg';
import { ReactComponent as AddBrandIcon } from '../../assets/imgs/icons/new-brand-icon.svg';
import { ReactComponent as ExitIcon } from '../../assets/imgs/icons/exit-icon.svg';
import BrandModal from '../modal/BrandModal';
import { useAuth } from '../../hooks/auth';
import { useNavigate } from 'react-router-dom';
import BrandOption from '../modal/BrandOption';

export default function Sidemenu(props) {
    const url = "";
    const method = "POST"

    const [isBrandModalOpen, setBrandModalOpen] = useState(false);
    const openBrandModal = () => {
        setBrandModalOpen(!isBrandModalOpen);
    }

    const closeBrandModal = () => {
        setBrandModalOpen(false);
    }

    const auth = useAuth();
    const navigate = useNavigate();
    const handleLogout = () => {
        localStorage.removeItem("accessToken");
        auth.logout();
        navigate('/');
    }

    const handleBrandChange = (e) => {
        props.setSelectedBrand(e.target.value);
    }

    return (
        <SidemenuContainer isMobile={props.isMobile}>
            <SearchBar
                handleSearch={props.handleSearch}
                action={url}
                method={method}
                isMobile={props.isMobile}
            />
            <ul>
                <a onClick={props.openModal} role="button">
                    <li>
                        <AddIcon fill={props.fill} /> Add product
                    </li>
                </a>
                <a onClick={props.handleAllItemView}>
                    <li>
                        <ManageIcon fill={props.fill} /> Inventory
                    </li>
                </a>
                <a onClick={openBrandModal}>
                    <li>
                        <AddBrandIcon fill={props.fill} /> Create new brand
                    </li>
                </a>
                <a>
                    <div className='select-container'>
                        <BrandOption
                            onChange={handleBrandChange}
                            value={props.selectedBrand}
                            label="Filter by brand"
                        />
                        <button onClick={props.filterByBrand}>Search</button>
                    </div>
                </a>
                <a onClick={handleLogout}>
                    <li>
                        <ExitIcon fill={props.fill} /> Log out
                    </li>
                </a>
            </ul>
            <BrandModal
                closeModal={closeBrandModal}
                isBrandModalOpen={isBrandModalOpen}
            />
        </SidemenuContainer>
    );
}