import styled from "styled-components"

export const Header = styled.div`
    text-align: end;
    display: flex;
    justify-content: space-between;
    p {
        font-size: 10vmin;
        font-weight: 300;
    }

    img {
        cursor: pointer;
        opacity: 0;

        @media screen and (max-width: 425px) {
            opacity: 1;
        }
    }
`

export const ManagementContainer = styled.div`
    position: relative;
    top: 0;
    left: 0;
    width: 100%;
    text-align: end;
    padding: 1.5% 3%;
    height: 100%;
    min-height: 50rem;
`

export const ProductContainer = styled.div`
    overflow-y: auto;
    max-height: 40rem;
    height: 100%;
    max-width: 100vmin;
    width: 100%;
    margin-top: 1%;
    display: flex;
    flex-wrap: wrap;
    justify-content: end;

    #empty-announce {
        font-size: 4vmin;
        font-weight: 300;
    }

    @media screen and (min-width: 1440px) {
        max-width: 100vw;
    }
`

export const InnerContainer = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    min-height: 45rem;
    height: 100%;
`

export const StyledSearchBar = styled.div`
    margin-bottom: 2rem;

    button {
        border: none;
        outline: none;
        background-color: transparent;
        aspect-ratio: 1/1;
    }

    input {
        max-width: 100%;
        margin-left: 1.4%;
        outline: none;
        border: none;
        border-bottom: 1px solid #000000;
        background-color: #ffffff;
        font-size: 2vmin;
        font-weight: 300;
        padding:1% 2%;
        color: #000000
    }

    svg {
        width: 4vmin;
        aspect-ratio: 1/1;
        opacity: 0.8;
        transition: opacity 200ms ease-in-out;
    }

    svg:hover {
        opacity: 1;
    }

    @media screen and (max-width: 768px) {
        input {
            font-size: 6vmin !important;
            width: 100%;
        }

        svg {
            width: 8vmin;
        }

    }
`

export const SidemenuContainer = styled.div`
    text-align: start;
    width: 30%;
    height: 20rem;
    transition: transform 200ms ease-in-out;
    background-color: ${(props) => props.isMobile ? "#000000" : "#ffffff"};

    div {
        margin-bottom: 2rem;

        button {
            border: none;
            outline: none;
            background-color: transparent;
            aspect-ratio: 1/1;
        }

        input {
            margin-left: 1.4%;
            outline: none;
            border: none;
            border-bottom: 1px solid #000000;
            font-size: 2vmin;
            font-weight: 300;
            padding:1% 2%;
        }
    }

    ul {
        padding: 0;
        list-style: none;
        text-align: start;
    }

    li {
        margin-bottom: 1.5%;
        cursor: pointer;
        font-size: 2vmin;
        opacity: 0.7;
        transition: opacity 150ms ease-in;

        svg {
            width: 7vmin;
            aspect-ratio: 1/1;
        }
    }

    li:hover {
        opacity: 1;
    }

    .select-container {
        width: 100%;
        margin-top: 0.6rem;

        select {
            width: 80%;
        }

        #options {
            margin-bottom: 0;
        }

        button {
            margin-top: 0.2rem;
            border: 1px solid #000000;
            padding: 0.2rem;
            width: 80%;
            aspect-ratio: unset;
            transition: 150ms ease-in-out;

            &:hover {
                background-color: #000000;
                color: #ffffff;
            }
        }

        @media screen and (max-width:768px) {
            #options {
                font-size: 4vmin;
            }

            select{
                font-size: 3vmin;
            }

            button {
                font-size: 3vmin;
            }
            
        }

        @media screen and (max-width:425px) {
            #options {
                font-size: 5vmin;
            }

            select{
                font-size: 5vmin;
            }

            button {
                font-size: 5vmin;
            }
        }
    }

    @media screen and (max-width:768px) {
        & {
            width: 50%;
        }

        li {
            font-size: 4vmin;
        }
    }

    @media screen and (max-width:425px) {
        position: absolute;
        border: 3px solid #000000;
        background-color: #ffffff;
        padding: 5%;
        width: 90%;
        border-radius: 10px;
        height: fit-content;
        z-index: 100;
        transform: translateX(${(props) => props.isMobile ? "0%" : "-120%"});
        transition: transform 200ms ease-in-out;

        li {
            font-size: 5vmin;
        }
    }
`

export const ProductDetailContainer = styled.div`
    max-width: 20rem;
    /* max-height: 33rem; */
    height: fit-content;
    width: 100%;
    aspect-ratio: 1/1;
    padding: 3%;
    border: 2px solid #000000;
    border-radius: 10px;
    text-align: start;
    margin-left: 2%;
    margin-bottom: 1%;

    button {
        padding: 1%;
        /* max-width: 27vmin; */
        width: 100%;
        background-color: transparent;
        border: 1px solid;
        font-size: 2vmin;
        font-weight: 300;
        margin-bottom: 5%;
        opacity: 0.9;
        transform: scaleX(0.95);
        transition: all 200ms ease-in-out;
    }

    button:hover {
        transform: scaleX(1);
        opacity: 1;
    }

    .deleteBtn {
        border-color: #d91c1c;
        color: #d91c1c;
    }

    .productWrapper {
        margin-bottom: 3%;
        height: 23rem;
    }

    @media screen and (max-width: 768px) {
        & {
            max-width: 100%;
        }

        button {
            font-size: 4vmin;
        }
    }

    @media screen and (max-width: 375px) {

        button {
            font-size: 5vmin;
        }
    }
`

export const ProductImg = styled.img`
    max-width: 26vmin;
    width: 100%;
    aspect-ratio: 1/1;
    border-radius: 10px;
    margin-bottom: 5%;

    @media screen and (max-width: 375px) {
        & {
            max-width: 43vmin;
            padding: 1%;
        }
    }
`

export const ProductInfoContainer = styled.div`
    width: 100%;

    h2 {
        font-size: 4vmin;
    }

    p {
        font-size: 2vmin;
        font-weight: 300;
        margin-bottom: 0.5rem;
    }

    .desc {
        max-height: 8rem;
        height: 100%;
        overflow-y: auto;
    }

    @media screen and (max-width: 768px) {
        h2 {
            font-size: 4vmin;
        }

        p {
            font-size: 3vmin;
        }
    }

    @media screen and (max-width: 375px) {
        h2 {
            width: 100%;
            font-size: 8vmin;
        }

        p {
            width: 100%;
            font-size: 5vmin;
        }
    }
`