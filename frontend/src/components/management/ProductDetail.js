import React, { useState } from 'react'
import { Link } from 'react-router-dom';
import { ProductDetailContainer, ProductInfoContainer } from './Management.style';

export default function ProductDetail(props) {
    const [isEditFormOpen, setFormOpen] = useState(false);
    const toggleEditForm = () => {
        setFormOpen(!isEditFormOpen);
    }

    return (
        <ProductDetailContainer>
            <ProductInfoContainer>
                <h2>{props.productName}</h2>
                <p><strong>ID: </strong>{props.productId}</p>
                <p><strong>Brand: </strong>{props.productBrand}</p>
                <p><strong>Capacity: </strong>{props.productCapacity}</p>
                <p><strong>Price: </strong>{props.productPrice}$</p>
            </ProductInfoContainer>

            <Link to={"/manage/" + props.productId}>
                <button>Detail</button>
            </Link>

            <button onClick={props.handleDelete} className="deleteBtn" id={props.productId}>Delete</button>
        </ProductDetailContainer>
    );
}