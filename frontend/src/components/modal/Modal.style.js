import styled, { keyframes } from "styled-components"

export const StyledCreateModal = styled.div`
    position: absolute;
    top: 4%;
    left: 50%;
    transform: translateX(-50%);
    display: flex;
    flex-direction: column;
    width: 100%;
    background-color: #ffffff;
    border: 3px solid #000000;
    padding: 5% 15%;
    border-radius: 15px;
    z-index: 100;

    h1 {
        font-size: 4.5rem;
        text-transform: uppercase;
        margin-bottom: 2rem;
    }

    #uploadFile {
        border: none;
        padding: 2% 0;
    }

    textarea {
        resize: none;
        font-size: 2vmin;
        width: 100%;
        height: 15rem;
        padding: 1%;
        border: 1px solid #000000;
        font-weight: 300;
    }

    label {
        font-size: 3vmin;
    }
    
    input {
        width: 100%;
        font-size: 2vmin;
        padding: 1% 2%;
        margin-bottom: 2%;
        border: none;
        font-weight: 300;
        border-bottom: 1px solid #000000;
    }
    
    input:focus {
        outline: none;
    }

    select {
        font-size: 2vmin;
        width: 100%;
        padding: 1%;
        border: 1px solid #000000;
        margin-bottom: 2%;
    }

    #file-input {
        padding: 0;
        border: none;
        margin-top:1.2rem
    }

    .submitButtonContainer button {
        margin-top: 1.5rem;
        padding: 1%;
        font-size: 3vmin;
        width: 100%;
        background-color: transparent;
        transition: all 200ms ease-in-out;

        &:hover {
            background-color: #000000;
            color: #ffffff;
        }
    }

    .buttonContainer {
        text-align: end;
        padding-bottom: 4%;
        button {
            background-color: transparent;
            width: 5vmin;
            border: none;

            img {
                width: 4vmin;
                aspect-ratio: 1/1;
            }
        }
    }

    @media screen and (max-width: 426px) {
        & {
            width: 90vmin;
        }

        label {
            font-size: 5vmin;
        }

        input {
            font-size: 5vmin !important;
        }

        textarea {
            font-size: 4vmin;
        }

        .submitBtn {
            width: 26vmin !important;
            font-size: 4vmin !important;
        }
    }

`

export const Overlay = styled.div`
    position: absolute;
    z-index: 99;
    top: 0;
    left: 0;
    width: 100vw;
    height: 120vh;
    background-color: #000000;
    opacity:0.3;
`

const slideFadeInKeyframe = keyframes`
    from {
        opacity: 0;
        transform: translateX(-100%);
    }

    to {
        opacity: 1;
        transform: translateX(0);
    }
`

export const StyledBrandModal = styled.div`
    z-index: 100;
    position: relative;
    top: 0;
    left: 0;
    width: 100%;
    height: 70%;
    border: 2px solid #000000;
    padding: 1rem;
    border-radius: 10px;
    animation: ${slideFadeInKeyframe} 200ms ease-in;

    p {
        color: red;
    }

    /* close button */
    .buttonContainer {
        margin-bottom: 0.3rem;
        button{
            margin-bottom: 0.3rem;
            img {
                transform: scale(0.5);
            }
        }
    }

    .brandSubmitButtonContainer {
        text-align: center;
        margin-top: 8%;
        .submitBtn {
            position: relative;
            background-color: transparent;
            padding: 0.2rem;
            font-size: 2vmin;
            border: 1px solid #000000;
            width: 100%;
            height: 3rem;

            &::before {
                content: '';
                position: absolute;
                top: 0;
                left: 0;
                right: 0;
                bottom: 0;
                transform-origin: right;
                transform: scaleX(0);
                background-color: #000000;
                transition: transform 250ms ease-in-out;
                z-index: -1;
            }

            &:hover::before {
                transform: scaleX(1);
                transform-origin: left;
            }

            &:hover{
                color: #ffffff
            }
        }

    }

    input {
        width: 100%;
    }

    label {
        font-size: 3vmin;
        width: 100%;
    }

    @media screen and (max-width:425px) {
        .brandSubmitButtonContainer {
            .submitBtn {
                padding: 0;
                font-size: 3vmin;
            }
        }

        label {
            font-size: 4.5vmin;
        }

        input {
            font-size: 3.5vmin !important;
        }
    }
`