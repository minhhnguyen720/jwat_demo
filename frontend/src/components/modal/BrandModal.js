import React, { useState } from 'react'
import CloseButton from '../backButton/CloseButton';
import InputField from './InputField'
import { Overlay, StyledBrandModal } from './Modal.style'

export default function BrandModal(props) {
    const [fieldValue, setFieldValue] = useState("");
    const [failure,setFailure] = useState(false);

    const handleChange = (e) => {
        const { value } = e.target;
        setFailure(false);
        setFieldValue(value);
    }

    const submitForm = (e) => {
        e.preventDefault();

        const brand = {
            "name": fieldValue
        }

        let myHeaders = new Headers();
        myHeaders.append("Authorization", localStorage.getItem("accessToken"));
        myHeaders.append('Content-Type', 'application/json' );

        fetch("http://localhost:8080/api/brand", {
            method: "POST",
            headers: myHeaders,
            body: JSON.stringify(brand)
        }).then(res => {
            if(res.ok){
                setFieldValue("");
            }
            else{
                setFailure(true);
            }
            
        })
    }

    if (!props.isBrandModalOpen) return;

    return (
        <>
            <StyledBrandModal isBrandModalOpen={props.isBrandModalOpen}>
                <CloseButton closeModal={props.closeModal} />
                {failure && <p>Already exist</p>}
                <InputField
                    label="Brand name"
                    onChange={handleChange}
                    type="text"
                    placeholder="Brand name"
                    value={fieldValue}
                />
                <div className="brandSubmitButtonContainer">
                    <button onClick={submitForm} className="submitBtn" id="createBrandBtn" type="submit">Create</button>
                </div>
            </StyledBrandModal>
        </>
    )
}
