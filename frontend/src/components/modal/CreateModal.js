import React, { useState } from 'react'
import { Overlay, StyledCreateModal } from './Modal.style'
import BrandOption from './BrandOption';
import InputField from './InputField';
import CloseButton from '../backButton/CloseButton';
import Textarea from './Textarea';
import Axios from 'axios'

export default function CreateModal(props) {

  const [item, setItem] = useState({
    name: "",
    price: 0,
    capacity: 0,
    brand: "1", // the type must be String so that server can find for appropriate brand
    description: "",
    url: ""
  })

  const [option, setOption] = useState(1);
  const handleOptionChange = (e) => {
    const { value } = e.target;
    setOption(value);
  }

  const handleChange = (e) => {
    const { name, value } = e.target;

    setItem((prevValue) => {
      return {
        ...prevValue,
        [name]: value
      };
    });
  }

  // HANDLE UPLOAD ITEM
  const [selectedImg, setSelectedImg] = useState('');
  let myHeaders = new Headers();
  myHeaders.append("Authorization", localStorage.getItem("accessToken"));
  myHeaders.append('Content-Type', 'application/json');

  const getCloudName = async () => {
    // upload image to Cloudinary
    const cloudNameStream = await fetch("http://localhost:8080/api/cloudinary/cloudName", {
      method: "GET",
      headers: myHeaders
    });

    const cloudName = atob(await cloudNameStream.text());
    return cloudName;
  }
  const getUploadPreset = async () => {
    const uploadPresetStream = await fetch("http://localhost:8080/api/cloudinary/preset", {
      method: "GET",
      headers: myHeaders
    });
    const uploadPreset = atob(await uploadPresetStream.text())
    return uploadPreset;
  }

  const getFormData = async (uploadPreset) => {
    const formData = new FormData();
    formData.append("file", selectedImg);
    formData.append("upload_preset", uploadPreset);

    return formData;
  }

  // const [publicId, setPublicId] = useState("");
  let model2send = {
    name: "",
    price: 0,
    capacity: 0,
    brand: "1", // the type must be String so that server can find for appropriate brand
    description: "",
    url: ""
  }
  const uploadImage = async (cloudName, uploadPreset) => {
    const formData = await getFormData(uploadPreset);
    const url = `https://api.cloudinary.com/v1_1/${cloudName}/image/upload`;
    await fetch(url, {
      method: "POST",
      body: formData
    }).then(res => { return res.json() })
      .then(data => {
        model2send = {
          name: item.name,
          price: item.price,
          capacity: item.capacity,
          brand: option.toString(), // the type must be String so that server can find for appropriate brand
          description: item.description,
          url: data.public_id
        }
      })
  }

  const submitForm = async () => {
    const cloudName = await getCloudName();
    const uploadPreset = await getUploadPreset();
    await uploadImage(cloudName, uploadPreset);
    /**
     * Inspect formData
     */
    // for (var pair of formData.entries()) {
    //   console.log(pair[0] + ', ' + pair[1]);
    // }
    await fetch("http://localhost:8080/api/item", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "Authorization": localStorage.getItem("accessToken")
      },
      body: JSON.stringify(model2send)
    }).then(
      model2send = {
        name: "",
        price: 0,
        capacity: 0,
        brand: "1", // the type must be String so that server can find for appropriate brand
        description: "",
        url: ""
      }
    )

    setItem({
      name: "",
      price: 0,
      capacity: 0,
      brand: "1",
      description: "",
      url: ""
    })

    props.closeModal();

  }
  //---------------------------------------------

  if (!props.isOpen) return;

  return (
    <>
      {/* <Overlay /> */}
      <StyledCreateModal>
        <CloseButton closeModal={props.closeModal} />
        <h1>Create new item</h1>
        <InputField
          label="Name"
          onChange={handleChange}
          type="text"
          name="name"
          placeholder="Name"
          value={item.name}
        />

        <BrandOption
          value={option}
          onChange={handleOptionChange}
          label="Brands"
        />

        <InputField
          label="Price"
          onChange={handleChange}
          type="number"
          name="price"
          value={item.price}
        />

        <InputField
          label="Capacity"
          onChange={handleChange}
          type="number"
          name="capacity"
          value={item.capacity}
        />

        <Textarea
          label="Description"
          onChange={handleChange}
          name="description"
          placeholder="Description"
          value={item.description}
        />

        <input
          id='file-input'
          type='file'
          onChange={(e) => {
            setSelectedImg(e.target.files[0]);
          }}
        />

        <div className="submitButtonContainer">
          <button onClick={submitForm} type="submit">{props.btnLabel}</button>
        </div>

      </StyledCreateModal>
    </>
  );
}
