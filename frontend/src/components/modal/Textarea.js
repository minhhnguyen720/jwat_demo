import React from 'react'

export default function Textarea(props) {
    return (
        <>
            <label>{props.label}</label>
            <textarea
                onChange={props.onChange}
                type="text"
                name={props.name}
                placeholder={props.placeholder}
                value={props.value}
            />
        </>
    );
}