import React, { useEffect, useState } from 'react'
import styled from "styled-components"

const StyledSelect = styled.select`
    padding: 2%;
`

const OptionHeader = styled.div`
    margin-bottom: 2%;
    display: flex;
    justify-content: space-between;

    button {
        padding: 2%;
        background-color: #ffffff;
        border: none;
        opacity: 0.7;
        transition: opacity 200ms ease-in-out;
    }

    button:hover {
        opacity: 1;
    }
`

export default function BrandOption(props) {
    const [brandData, setBrandData] = useState([]);
    let myHeaders = new Headers();
    myHeaders.append("Authorization", localStorage.getItem("accessToken"));

    let requestOptions = {
        method: 'GET',
        headers: myHeaders,
        redirect: 'follow'
    };
    const getBrandData = async () => {
        const response = await fetch('http://localhost:8080/api/brand',requestOptions);
        const dataFromDB = await response.json();

        const header = {id:"",name:"Select brand"}
        const brandArr = dataFromDB;
        brandArr.unshift(header);
        setBrandData(brandArr);
    }

    useEffect(() => {
        getBrandData();
    }, [])

    return (
        <>
            <OptionHeader id='options'>
                <label>{props.label}</label>
            </OptionHeader>
            <StyledSelect value={props.value} onChange={props.onChange}>
                {
                    brandData.map((brand) => {
                        return (
                            <option key={brand.id} value={brand.id}>{brand.name}</option>
                        );
                    })
                }
            </StyledSelect>
        </>
    );
}