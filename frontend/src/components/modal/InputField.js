import React from 'react'

export default function InputField(props) {
    return (
        <>
            <label>{props.label}</label>
            <input
                onChange={props.onChange}
                type={props.type}
                name={props.name}
                placeholder={props.placeholder}
                value={props.value}
            />
        </>
    );
}