import React, { useState } from 'react';
import { Overlay, StyledCreateModal } from './Modal.style'
import CloseButton from '../backButton/CloseButton';
import InputField from './InputField';
import Textarea from './Textarea';
import BrandOption from './BrandOption';

export default function EditModal(props) {
    const [itemValue, setItemValue] = useState({
        itemName: props.item.name,
        itemDescription: props.item.desc,
        itemPrice: props.item.price,
        itemCapacity: props.item.capacity,
        itemBrandId: props.item.brand
    })

    const [option, setOption] = useState("");
    const handleOptionChange = (e) => {
      const { value } = e.target;
      setOption(value);
    }

    const submitForm = () => {
        const item = {
            name: itemValue.itemName,
            description: itemValue.itemDescription,
            price: itemValue.itemPrice,
            itemCapacity: props.itemCapacity,
            brandId: itemValue.itemBrandId
        }

        fetch(`http://localhost:8080/api/item/${props.item.id}`, {
            method: "PUT",
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(item)
        })

        document.location.reload();
        props.closeModal();
    }

    const handleChange = (e) => {
        const { name, value } = e.target;

        setItemValue((prevValue) => {
            return {
                ...prevValue,
                [name]: value
            };
        });
    }

    if (!props.isOpen) return;

    return (
        <>
            <Overlay />
            <StyledCreateModal>
                <CloseButton closeModal={props.closeModal} />

                <InputField
                    label="Name"
                    onChange={handleChange}
                    type="text"
                    name="itemName"
                    placeholder="Name"
                    value={itemValue.itemName}
                />

                {/* <BrandOption value={option} onChange={handleOptionChange} /> */}

                <InputField
                    label="Price"
                    onChange={handleChange}
                    type="number"
                    name="itemPrice"
                    value={itemValue.itemPrice}
                />

                <InputField
                    label="Capacity"
                    onChange={handleChange}
                    type="number"
                    name="itemCapacity"
                    value={itemValue.itemCapacity}
                />

                <Textarea
                    label="Description"
                    onChange={handleChange}
                    name="itemDescription"
                    placeholder="Description"
                    value={itemValue.itemDescription}
                />

                <div className="submitButtonContainer">
                    <button onClick={submitForm} className="submitBtn" type="submit">{props.btnLabel}</button>
                </div>
            </StyledCreateModal>
        </>
    );
}
