package com.jwat.server.user;

import java.util.Arrays;
import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping(path = "api/users")
public class UserController {
    private static final List<User> users = Arrays.asList(
        new User(1, "James Bond"),
        new User(2, "Maria Jones"),
        new User(3, "Anna Smith")
      );
  
      @GetMapping(path = "{id}")
      public User getUsers(@PathVariable("id") Integer id) {
          return users.stream()
                  .filter(user -> id.equals(user.getId()))
                  .findFirst()
                  .orElseThrow(() -> new IllegalStateException(
                          "User " + id + " does not exists"
                  ));
      }
}
