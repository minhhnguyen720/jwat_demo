package com.jwat.server.Size;

import java.util.*;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.springframework.web.bind.annotation.CrossOrigin;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jwat.server.Item.Item;

import static javax.persistence.GenerationType.*;

@Entity(name = "Size")
@CrossOrigin
@Table(name = "size")
public class Size {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "size_id", nullable = false, updatable = false)
    private Long id;

    @Column(name = "value")
    private String value;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "sizes")
    private Set<Item> items = new HashSet<>();

    public Size() {
    }

    public Size(String value) {
        this.value = value;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getValue() {
        return this.value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Set<Item> getItems() {
        return this.items;
    }

    public void assignItem(Item item) {
        items.add(item);
    }
}
