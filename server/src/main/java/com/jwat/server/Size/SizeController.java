package com.jwat.server.Size;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping(path = "api/size")
public class SizeController {

    @Autowired
    SizeService service;

    @GetMapping
    public List<Size> getSizes() {
        return service.getSizes();
    }

    @PostMapping
    public void createSize(@RequestBody Size size) {
        service.createSize(size);
    }

    @PutMapping(path = "update/{sizeId}/{itemId}")
    public void addItemToSize(@PathVariable Long sizeId, @PathVariable Long itemId) {
        service.addItemToSize(sizeId, itemId);
    }

    @PutMapping(value = "update/{sizeId}/remove/item/{itemId}")
        public void removeSizeFromItem(@PathVariable Long sizeId, @PathVariable Long itemId) {
            service.removeSizeFromItem(sizeId,itemId);
    }
}
