package com.jwat.server.Size;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface SizeRepository extends JpaRepository<Size,Long> {
    
    @Query("SELECT s FROM Size s WHERE s.value = ?1")
    Optional<Size> findSizeByValue(String value);
}
