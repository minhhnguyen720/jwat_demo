package com.jwat.server.Size;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jwat.server.Item.Item;
import com.jwat.server.Item.ItemServices;

@Service
public class SizeService {
    @Autowired
    private SizeRepository repo;

    @Autowired
    private ItemServices itemServices;

    public List<Size> getSizes() {
        return repo.findAll();
    }

    public void createSize(Size size) {
        Optional<Size> sizeOptional = repo.findSizeByValue(size.getValue());
        if (sizeOptional.isPresent()) {
            throw new IllegalStateException("Already exist");
        }
        repo.save(size);
    }

    public void addItemToSize(Long sizeId, Long itemId) {
        Item targetItem = itemServices.getItemById(itemId);
        Size targetSize = repo.findById(sizeId).orElseThrow(() -> new IllegalStateException("Size " + sizeId + " not found"));

        targetItem.assignSize(targetSize);
        targetSize.assignItem(targetItem);

        repo.save(targetSize);
        try {
            itemServices.updateItem(itemId, targetItem);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void removeSizeFromItem(Long sizeId, Long itemId) {
    }

}
