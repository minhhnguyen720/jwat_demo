package com.jwat.server.Item;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jwat.server.brand.Brand;
import com.jwat.server.brand.BrandRepository;
import com.jwat.server.Color.Color;
import com.jwat.server.Color.ColorRepository;
import com.jwat.server.Size.Size;
import com.jwat.server.Size.SizeRepository;

import javax.transaction.Transactional;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

@Service
public class ItemServices {
    @Autowired
    private ItemRepository itemRepo;

    @Autowired
    private BrandRepository brandRepo;

    @Autowired
    private SizeRepository sizeRepo;

    @Autowired
    private ColorRepository colorRepo;

    public List<Item> getItems() {
        return itemRepo.findAll();
    }

    public void createNewItem(Item item) {
        Optional<Item> itemOptional = itemRepo.findItemByName(item.getName());
        if (itemOptional.isPresent()) {
            throw new IllegalStateException("Already exist");
        }

        Brand brand = brandRepo.findById(Long.parseLong(item.getBrand().getName())).get();

        item.setBrand(brand);

        itemRepo.save(item);
    }

    public void deleteItem(Long id) {
        boolean isExist = itemRepo.existsById(id);
        if (!isExist) {
            throw new IllegalStateException("Item " + id + " not found");
        }

        // prevent itemRepo deletes appropriate row of Brand table
        itemRepo.findById(id).get().setBrand(null);

        itemRepo.deleteById(id);
    }

    public List<Item> getItemByBrand(Long id) {
        return itemRepo.getItemByBrand(id);
    }

    @Transactional // if something goes wrong, rollback to previous action and the new data won't
                   // be stored in DB
    public void updateItem(Long id, Item item) throws IOException {

        Item tempItem = itemRepo.findById(id).orElseThrow(() -> new IllegalStateException("Item " + id + " not found"));

        if (item.getName() != null
                && item.getName().length() > 0
                && !Objects.equals(tempItem.getName(), item.getName())) {
            tempItem.setName(item.getName());
        }

        tempItem.setPrice(item.getPrice());
        tempItem.setCapacity(item.getCapacity());
        tempItem.setDescription(item.getDescription());

        itemRepo.save(tempItem);
    }

    public Item getItemById(Long id) {
        return itemRepo.findById(id).orElseThrow(() -> new IllegalStateException("Item " + id + " not found"));
    }

    public String getImgUrlById(Long id) {
        Item targetItem = itemRepo.findById(id).get();
        return targetItem.getUrl();
    }

    public void assignSize(Long id, Long sizeId) {
        Item targetItem = itemRepo.findById(id)
                .orElseThrow(() -> new IllegalStateException("Item " + id + " not found"));
        Size targetSize = sizeRepo.findById(sizeId)
                .orElseThrow(() -> new IllegalStateException("Size " + id + " not found"));
        ;

        targetItem.assignSize(targetSize);
        targetSize.assignItem(targetItem);

        itemRepo.save(targetItem);
        sizeRepo.save(targetSize);
    }

    public void assignColor(Long id, Long colorId) {
        Item targetItem = itemRepo.findById(id)
                .orElseThrow(() -> new IllegalStateException("Item " + id + " not found"));
        Color targetColor = colorRepo.findById(colorId)
                .orElseThrow(() -> new IllegalStateException("Color " + id + " not found"));

        targetItem.assignColor(targetColor);
        targetColor.assignItem(targetItem);

        itemRepo.save(targetItem);
        colorRepo.save(targetColor);
    }

    public Set<Size> getSizes(Long id) {
        Item item = itemRepo.findById(id).orElseThrow(() -> new IllegalStateException("Item " + id + " is not found"));
        return item.getSizes();
    }

    public void removeSize(Long itemId, Long sizeId) {
        Item item = itemRepo.findById(itemId)
                .orElseThrow(() -> new IllegalStateException("Item " + itemId + " is not found"));

        Set<Size> sizes = item.getSizes();

        sizes.forEach(size -> {
            if (size.getId() == sizeId) {
                sizes.remove(size);
                return;
            }
        });

        itemRepo.save(item);

    }

    public Item getPublicItemById(Long id) {
        Item item = itemRepo.findById(id).orElseThrow(() -> new IllegalStateException("Item " + id + " is not found"));

        Item publicItem = new Item(
                item.getName(),
                item.getPrice(),
                item.getBrand(),
                item.getDescription(),
                item.getUrl(),
                item.getSizes());

        return publicItem;
    }
}
