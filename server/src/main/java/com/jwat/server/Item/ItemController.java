package com.jwat.server.Item;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.jwat.server.Size.Size;

import java.io.IOException;
import java.util.List;
import java.util.Set;

@RestController
@CrossOrigin
@RequestMapping(path = "api/item")
public class ItemController {

    @Autowired
    private ItemServices itemServices;

    @GetMapping
    public List<Item> getItems() {
        return itemServices.getItems();
    }

    @GetMapping(path = "{id}")
    public Item getItemById(@PathVariable Long id) {
        return itemServices.getItemById(id);
    }

    @GetMapping(path = "{id}/sizes")
    public Set<Size> getSizes(@PathVariable Long id) {
        return itemServices.getSizes(id);
    }

    @GetMapping(path = "{id}/img")
    public String getImgUrlById(@PathVariable Long id) {
        return itemServices.getImgUrlById(id);
    }

    @GetMapping(path = "public/brand/{id}")
    public List<Item> getItemByBrand(@PathVariable Long id) {
        return itemServices.getItemByBrand(id);
    }

    @PostMapping
    public void createNewItem(@RequestBody Item item) {
        itemServices.createNewItem(item);
    }

    @DeleteMapping(path = "{id}")
    public void deleteItem(@PathVariable Long id) {
        itemServices.deleteItem(id);
    }

    @PutMapping(path = "{id}")
    public void updateItem(
            @PathVariable Long id,
            @RequestBody Item item
    ) throws IOException {
        itemServices.updateItem(id,item);
    }

    @PutMapping(path= "{id}/size/{sizeId}")
    public void assignSize(
        @PathVariable Long id,
        @PathVariable Long sizeId
    ) {
        itemServices.assignSize(id,sizeId);
    }

    @PutMapping(path = "{itemId}/size/remove/{sizeId}")
    public void removeSize(@PathVariable Long itemId, @PathVariable Long sizeId) {
        itemServices.removeSize(itemId,sizeId);
    }

    @PutMapping(path= "{id}/color/{colorId}")
    public void assignColor(
        @PathVariable Long id,
        @PathVariable Long colorId
    ) {
        itemServices.assignColor(id,colorId);
    }
}
