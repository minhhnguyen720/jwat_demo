package com.jwat.server.Item;

import javax.persistence.*;

import org.springframework.web.bind.annotation.CrossOrigin;

import com.jwat.server.brand.Brand;
import com.jwat.server.Color.Color;
import com.jwat.server.Size.Size;

import static javax.persistence.GenerationType.*;

import java.util.*;

@Entity(name = "Item")
@CrossOrigin
@Table(name = "item")
public class Item {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "item_id", updatable = false)
    private Long id;

    @Column(name = "name", nullable = false, columnDefinition = "TEXT")
    private String name;

    @Column(name = "price", nullable = false)
    private float price;

    @Column(name = "capacity", nullable = true)
    private int capacity;

    @Column(name = "description", nullable = false, length = 255)
    private String description;

    @Column(name = "url", nullable = true, length = 255)
    private String url;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "brand")
    private Brand brand;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "item_size", joinColumns = { @JoinColumn(name = "item_id") }, inverseJoinColumns = {
            @JoinColumn(name = "size_id") })
    private Set<Size> sizes = new HashSet<>();

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "item_colors", joinColumns = { @JoinColumn(name = "item_id") }, inverseJoinColumns = {
            @JoinColumn(name = "color_id") })
    private Set<Color> colors = new HashSet<>();

    public Item() {
    }

    public Item(
            String name,
            float price,
            int capacity,
            Brand brand,
            String description,
            String url) {
        this.name = name;
        this.price = price;
        this.capacity = capacity;
        this.brand = brand;
        this.description = description;
        this.url = url;
    }

    public Item(
            String name,
            float price,
            int capacity,
            Brand brand,
            String description) {
        this.name = name;
        this.price = price;
        this.capacity = capacity;
        this.brand = brand;
        this.description = description;
    }

    public Item(
            String name,
            float price,
            Brand brand,
            String description,
            String url,
            Set<Size> sizes) {
        this.name = name;
        this.price = price;
        this.brand = brand;
        this.description = description;
        this.url = url;
        this.sizes = sizes;
    }

    // Getter and Setter
    public Brand getBrand() {
        return brand;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<Size> getSizes() {
        return this.sizes;
    }

    public void setSizes(Set<Size> sizes) {
        this.sizes = sizes;
    }

    public void assignSize(Size size) {
        sizes.add(size);
    }

    public void assignColor(Color color) {
        colors.add(color);
    }

    public Set<Color> getColors() {
        return this.colors;
    }

    public void setColors(Set<Color> colors) {
        this.colors = colors;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public int getCapacity() {
        return capacity;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "{" +
                " id='" + getId() + "'" +
                ", name='" + getName() + "'" +
                ", price='" + getPrice() + "'" +
                ", capacity='" + getCapacity() + "'" +
                ", description='" + getDescription() + "'" +
                ", url='" + getUrl() + "'" +
                ", brand='" + getBrand() + "'" +
                ", sizes='" + getSizes() + "'" +
                ", colors='" + getColors() + "'" +
                "}";
    }

}
