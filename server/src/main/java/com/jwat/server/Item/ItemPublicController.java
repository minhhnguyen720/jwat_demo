package com.jwat.server.Item;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jwat.server.Size.Size;

@RestController
@RequestMapping(path = "api/public/item")
public class ItemPublicController {
    @Autowired
    ItemServices itemServices;

    @GetMapping(path="{id}")
    public Item getPublicItemById(@PathVariable Long id) {
        return itemServices.getPublicItemById(id);
    }

    @GetMapping(path = "{id}/sizes")
    public Set<Size> getSizes(@PathVariable Long id) {
        return itemServices.getSizes(id);
    }
}
