package com.jwat.server.Item;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ItemRepository extends JpaRepository<Item, Long> {
    @Query("SELECT i FROM Item i WHERE i.name = ?1")
    Optional<Item> findItemByName(String name);

    @Query(value = "SELECT * FROM Item i join Brand b on i.brand = b.id WHERE b.id = ?1", nativeQuery = true)
    List<Item> getItemByBrand(Long id);
}
