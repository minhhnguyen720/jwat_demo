package com.jwat.server.brand;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;


@RestController
@CrossOrigin
@RequestMapping("api/brand")
public class BrandController {
    @Autowired
    private BrandService service;

    @GetMapping
    public List<Brand> getBrands() {
        return service.getBrands(); 
    }

    @PostMapping
    public ResponseEntity<String> createBrand(@RequestBody Brand brand) {
        return service.createBrand(brand);
    }

    // @PutMapping(path = "/update/{brandId}/{itemId}")
    // public void addItemToBrand(@PathVariable Long brandId, @PathVariable Long itemId) {
    //     service.addItemToBrand(brandId, itemId);
    // }
    
}
