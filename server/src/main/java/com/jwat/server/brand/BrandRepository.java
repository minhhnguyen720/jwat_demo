package com.jwat.server.brand;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface BrandRepository extends JpaRepository<Brand, Long> {
    @Query("SELECT i FROM Brand i WHERE i.name = ?1")
    Optional<Brand> findBrandByName(String name);

}
