package com.jwat.server.brand;

// import java.io.IOException;
// import java.util.Optional;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

// import com.jwat.server.item.Item;
// import com.jwat.server.item.ItemServices;

@Service
public class BrandService {

    @Autowired
    private BrandRepository repo;

    // @Autowired
    // private ItemServices itemServices;

    public List<Brand> getBrands() {
        return repo.findAll();
    }

    public ResponseEntity<String> createBrand(Brand brand) {
        Optional<Brand> optionBrand = repo.findBrandByName(brand.getName());
        System.out.println(optionBrand);
        System.out.println(optionBrand.isPresent());
        if (optionBrand.isPresent()) {
            return new ResponseEntity<>("Already exist",HttpStatus.BAD_REQUEST);
        }
        repo.save(brand);
        
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
