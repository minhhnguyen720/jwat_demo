package com.jwat.server.Color;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping(path = "api/color")
public class ColorController {
    
    @Autowired
    private ColorService service;

    @GetMapping
    public List<Color> getColors() {
        return service.getColors();
    }

    @PostMapping
    public void createColor(@RequestBody Color color) {
        service.createColor(color);
    }

    @PutMapping(path = "update/{colorId}/{itemId}")
    public void addItemToSize(@PathVariable Long colorId, @PathVariable Long itemId) {
        service.addItemToColor(colorId,itemId);
    }
}
