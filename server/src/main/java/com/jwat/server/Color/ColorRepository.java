package com.jwat.server.Color;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ColorRepository extends JpaRepository<Color, Long> {
    
    @Query("SELECT c FROM Color c WHERE c.value = ?1")
    Optional<Color> findColorByValue(String value);
}
