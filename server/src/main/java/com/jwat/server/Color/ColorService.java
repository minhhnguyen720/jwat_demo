package com.jwat.server.Color;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jwat.server.Item.Item;
import com.jwat.server.Item.ItemServices;

@Service
public class ColorService {

    @Autowired
    ColorRepository repo;

    @Autowired
    private ItemServices itemServices;

    public List<Color> getColors() {
        return repo.findAll();
    }

    public void createColor(Color color) {
        Optional<Color> colorOptional = repo.findColorByValue(color.getValue());
        if (colorOptional.isPresent()) {
            throw new IllegalStateException("Already exist");
        }
        repo.save(color);
    }

    public void addItemToColor(Long colorId, Long itemId) {
        Item targetItem = itemServices.getItemById(itemId);
        Color targetColor = repo.findById(colorId)
                .orElseThrow(() -> new IllegalStateException("Size " + colorId + " not found"));

        targetItem.assignColor(targetColor);
        targetColor.assignItem(targetItem);

        repo.save(targetColor);
        try {
            itemServices.updateItem(itemId, targetItem);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
