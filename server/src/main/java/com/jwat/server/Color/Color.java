package com.jwat.server.Color;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.springframework.web.bind.annotation.CrossOrigin;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jwat.server.Item.Item;

import static javax.persistence.GenerationType.*;

@Entity(name = "Color")
@CrossOrigin
@Table(name = "color")
public class Color {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "color_id", nullable = false, updatable = false)
    private Long colorId;

    @Column(name = "value", nullable = false)
    private String value;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "colors")
    private Set<Item> items = new HashSet<>();

    public Color(){};

    public Color(String value) {
        this.value = value;
    }

    public Long getId() {
        return this.colorId;
    }

    public String getValue() {
        return this.value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Set<Item> getItems() {
        return items;
    }

    public void assignItem(Item item) {
        items.add(item);
    }
}
