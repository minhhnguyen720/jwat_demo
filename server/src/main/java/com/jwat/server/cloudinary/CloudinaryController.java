package com.jwat.server.cloudinary;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "api/cloudinary")
public class CloudinaryController {
    @Autowired
    CloudinaryService cloudService;

    @GetMapping(path = "cloudName")
    public String getCloudName() {
        return cloudService.getCloudName();
    }

    @GetMapping(path = "preset")
    public String getPreset() {
        return cloudService.getPreset();
    }

}
