package com.jwat.server.cloudinary;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@ConfigurationProperties(prefix = "application.cloudinary")
@Configuration
public class CloudinaryConfig {
    private String uploadPreset;
    private String cloudName;

    public CloudinaryConfig(){}

    public String getCloudName() {
        return cloudName;
    }

    public String getUploadPreset() {
        return uploadPreset;
    }

    public void setCloudName(String cloudName) {
        this.cloudName = cloudName;
    }

    public void setUploadPreset(String uploadPreset) {
        this.uploadPreset = uploadPreset;
    }
}
