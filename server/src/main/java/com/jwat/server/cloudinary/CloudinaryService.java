package com.jwat.server.cloudinary;

import java.util.Base64;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CloudinaryService {
    @Autowired
    CloudinaryConfig config;

    public String getCloudName() {
        
        return Base64.getUrlEncoder().encodeToString(config.getCloudName().getBytes());
    }

    public String getPreset() {
        return Base64.getUrlEncoder().encodeToString(config.getUploadPreset().getBytes());
    }
    
}
