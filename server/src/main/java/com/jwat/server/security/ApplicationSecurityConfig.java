package com.jwat.server.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.jwat.server.jwt.JwtConfig;
import com.jwat.server.jwt.JwtSecretKey;
import com.jwat.server.jwt.JwtUsernameAndPasswordAuthenticationFilter;
import com.jwat.server.jwt.JwtVerifyTokenFilter;

@Configuration
@EnableWebSecurity
@CrossOrigin
@SuppressWarnings("deprecation")
public class ApplicationSecurityConfig extends WebSecurityConfigurerAdapter {

    private final PasswordEncoder passwordEncoder;
    private final JwtSecretKey jwtSecretKey;
    private final JwtConfig jwtConfig;

    @Autowired
    public ApplicationSecurityConfig(PasswordEncoder passwordEncoder, JwtSecretKey jwtSecretKey, JwtConfig jwtConfig) {
        this.passwordEncoder = passwordEncoder;
        this.jwtSecretKey = jwtSecretKey;
        this.jwtConfig = jwtConfig;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .cors().and()
                .csrf().disable()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .addFilter(new JwtUsernameAndPasswordAuthenticationFilter(authenticationManager(), jwtConfig,
                        jwtSecretKey))
                .addFilterAfter(new JwtVerifyTokenFilter(jwtSecretKey, jwtConfig),
                        JwtUsernameAndPasswordAuthenticationFilter.class)
                .authorizeRequests()
                .antMatchers("/", "index", "/css/*", "/js/*").permitAll()
                .antMatchers("/api/manage/**").hasRole(ApplicationUserRole.ADMIN.name())
                .antMatchers("/api/item/public/brand/**").permitAll()
                .antMatchers("/api/public/**").permitAll()
                .anyRequest()
                .authenticated();
    }

    @Override
    @Bean
    protected UserDetailsService userDetailsService() {
        UserDetails admin = User.builder()
                .username("admin")
                .password(passwordEncoder.encode("admin"))
                .roles(ApplicationUserRole.ADMIN.name())
                .build();

        // UserDetails customer = User.builder()
        //         .username("customer")
        //         .password(passwordEncoder.encode("customer"))
        //         .roles(ApplicationUserRole.CUSTOMER.name())
        //         .build();

        return new InMemoryUserDetailsManager(
                admin);
    }

}
