package com.jwat.server.security;

import java.util.Set;

import com.google.common.collect.Sets;

import static com.jwat.server.security.ApplicationRolePermission.*;

public enum ApplicationUserRole {
    ADMIN(Sets.newHashSet(PRODUCT_READ, PRODUCT_WRITE)),
    CUSTOMER(Sets.newHashSet(PRODUCT_READ));

    private final Set<ApplicationRolePermission> permissions;

    ApplicationUserRole(Set<ApplicationRolePermission> permissions) {
        this.permissions = permissions;
    }

    public Set<ApplicationRolePermission> getPermissions() {
        return permissions;
    }
}
