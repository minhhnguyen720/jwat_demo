package com.jwat.server.jwt;


public class UsernameAndPasswordAuthenticationRequest {
    private String username;
    private String password;

    public UsernameAndPasswordAuthenticationRequest() {}

    public String getPassword() {
        return password;
    }

    public String getUsername() {
        return username;
    }
}
