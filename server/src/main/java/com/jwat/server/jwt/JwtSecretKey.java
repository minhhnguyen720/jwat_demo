package com.jwat.server.jwt;

import javax.crypto.SecretKey;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.jsonwebtoken.security.Keys;

@Configuration
public class JwtSecretKey {
    @Autowired
    private final JwtConfig config;

    public JwtSecretKey(JwtConfig config) {
        this.config = config;
    }

    @Bean
    public SecretKey getSecretKey() {
        return Keys.hmacShaKeyFor(config.getSecretKey().getBytes());
    }

}
